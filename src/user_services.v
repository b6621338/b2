module main

import databases
import crypto.bcrypt
import time

// service_get_user_by_id returns a user by id
fn (mut app App) service_get_user_by_id(user_id string) !User {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	results := sql db {
		select from User where id == user_id.int()
	} or { panic(err) }

	user := results.first()

	return user
}

// service_update_user updates a user
fn (mut app App) service_update_user(user User) ! {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	query := '
	update users set firstname = ?, lastname = ?, email = ?, access_level = ?, updated_at = ?'
	param := [user.firstname, user.lastname, user.email, user.access_level.str(), time.now().str()]
	db.exec_param_many(query, param) or { panic(err) }
}

// service_auth authenticates a user
fn (mut app App) service_auth(email string, test_password string) !(int, string) {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	users := sql db {
		select from User where email == email
	}!
	if users.len == 0 {
		return error('user not found')
	}
	user := users.first()

	bcrypt.compare_hash_and_password(test_password.bytes(), user.password.bytes()) or {
		return error('Failed to auth user, ${err}')
	}
	return user.id, user.password
}

pub fn (mut admin Admin) check_auth () bool {
	token := admin.get_cookie('token') or { '0' }.int()
	if token == 0 {
		return false
	}

	return true
}
