module main

import vweb
import databases

// controller_login is a controller that handles the login request
['/controller/user/login'; post]
pub fn (mut app App) controller_login(email string, password string) vweb.Result {
	if email == '' {
		app.set_status(400, '')
		return app.text('Email cannot be empty')
	}
	if password == '' {
		app.set_status(400, '')
		return app.text('Password cannot be empty')
	}
	id, _ := app.service_auth(email, password) or {
		app.set_status(400, '')
		return app.text('error: ${err}')
	}

	mut red := databases.setup()

	red.set('user_id', id.str())

	app.set_status(201, '')
	return app.text(id.str())
}
