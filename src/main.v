module main

import vweb
import os
import databases
import net.smtp
import mails

struct App {
    vweb.Context
	vweb.Controller
	mut:
		client &smtp.Client [vweb_global]
}

struct Admin {
	vweb.Context
	mut:
		mapper map[string]map[string]int [vweb_global]
}

fn main() {
	mut db := databases.create_db_connection() or { panic(err) }

	sql db {
		create table User
		create table Reservation
		create table Restriction
		create table RoomRestriction
		create table Room
	} or { panic('error on create table: ${err}') }

	db.close() or { panic(err) }

	// mut app := &App{}
	//
	// app.service_add_rooms() or { panic(err) }
	// app.service_add_restrictions() or { panic(err) }

    vweb.run_at(new_app(), vweb.RunParams{
        port: 8082
    }) or { panic(err) }
}

fn new_app() &App {
	println('Starting mail listener...')
	mut client := mails.setup()

    mut app := &App{
		controllers: [
			vweb.controller('/admin', &Admin{})
		]
		client: client
	}
    // makes all static files available.
    app.mount_static_folder_at(os.resource_abs_path('.'), '/')
    // app.handle_static('templates', true)
    return app
}
