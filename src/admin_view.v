module main

import vweb
import time
import x

// admin_dashboard is the admin controller
['/dashboard'; get]
pub fn (mut admin Admin) admin_dashboard() vweb.Result {
	if !admin.check_auth() { return admin.redirect('/login') }

	return $vweb.html()
}

// admin_all shows all reservations
['/reservations-all'; get]
pub fn (mut admin Admin) admin_all() vweb.Result {
	if !admin.check_auth() { return admin.redirect('/login') }

	reservations := admin.service_all_reservations() or { panic(err) }

	return $vweb.html()
}

// admin_new shows all new reservations
['/reservations-new'; get]
pub fn (mut admin Admin) admin_new() vweb.Result {
	if !admin.check_auth() { return admin.redirect('/login') }

	reservations := admin.service_all_new_reservations() or { panic(err) }

	return $vweb.html()
}

// admin_show shows a reservation
['/reservations/:src/:id'; get]
pub fn (mut admin Admin) admin_show(src string, id int) vweb.Result {
	if !admin.check_auth() { return admin.redirect('/login') }

	res := admin.service_get_reservation_by_id(id) or { panic(err) }

	month := admin.query['m']
	year := admin.query['y']

	return $vweb.html()
}

// post_admin_show updates a reservation
['/reservations/:src/:id'; post]
pub fn (mut admin Admin) post_admin_show(src string, id int) vweb.Result {
	res := Reservation{
		id: id
		firstname: admin.get_cookie('first_name') or { panic(err) }
		lastname: admin.get_cookie('last_name') or { panic(err) }
		email: admin.get_cookie('email') or { panic(err) }
		phone: admin.get_cookie('phone') or { panic(err) }
	}
	admin.service_update_reservation(res) or { panic(err) }

	month := admin.form['m']
	year := admin.form['y']

	if year == '' {
		return admin.redirect('/admin/reservations-$src')
	} else {
		return admin.redirect('/admin/reservations-calendar?y=$year&m=$month')
	}
}

// admin_calender shows a calender of reservations
['/reservations-calendar'; get]
pub fn (mut admin Admin) admin_calender() vweb.Result {
	if !admin.check_auth() { return admin.redirect('/login') }

	mut now := time.now()

	if admin.query['y'] != '' {
		year := admin.query['y']
		month := admin.query['m']
		now = time.Time{
			year: year.int()
			month: month.int()
			day: 01
		}
	}

	next := add_date(now, 1)
	last := add_date(now, -1)

	next_month := next.month
	next_month_year := next.year

	last_month := last.month
	last_month_year := last.year

	this_month := now.month
	this_month_year := now.year
	this_month_name := now.custom_format('MMMM YYYY')

	first_of_month := time.Time{
		year: now.year
		month: now.month
		day: 01
	}
	last_of_month := time.Time{
		year: now.year
		month: now.month
		day: time.days_in_month(now.month, now.year) or { panic(err) }
	}

	rooms := admin.service_get_all_rooms() or { panic(err) }

	mut res_map := []map[string]int{}
	mut blk_map := []map[string]int{}

	for _, x in rooms {
		mut reservation_map := map[string]int{}
		mut block_map := map[string]int{}

		for d in first_of_month.day..last_of_month.day {
			f := time.Time{
				year: now.year
				month: now.month
				day: d
			}
			reservation_map[f.format()] = 0
			block_map[f.format()] = 0
		}

		restrictions := admin.service_get_restriction_for_room_by_id(x.id, first_of_month, last_of_month) or { panic(err) }

		for _, y in restrictions {
			if y.restriction_id == 1 {
				for d in y.start_date[8..10].int()..y.end_date[8..10].int() {
					f := time.Time{
						year: now.year
						month: now.month
						day: d
					}
					reservation_map[f.format()] = y.reservation_id
				}
			} else {
				sd := y.start_date[0..10]
				block_map['$sd 00:00'] = y.id
			}
		}
		res_map << reservation_map
		blk_map << block_map

	}

	return $vweb.html()
}

// process_reservation marks a reservation as processed
['/process-reservation/:src/:id']
pub fn (mut admin Admin) process_reservation(src string, id int) vweb.Result {
	if !admin.check_auth() { return admin.redirect('/login') }

	admin.service_update_reservation_processed(id) or { panic(err) }

	year := admin.query['y']
	month := admin.query['m']

	if year == '' {
		return admin.redirect('/admin/reservations-$src')
	} else {
		return admin.redirect('/admin/reservations-calendar?y=$year&m=$month')
	}
}

// delete_reservation deletes a reservation from the database
['/delete-reservation/:src/:id']
pub fn (mut admin Admin) delete_reservation(src string, id int) vweb.Result {
	if !admin.check_auth() { return admin.redirect('/login') }

	admin.service_delete_reservation(id) or { panic(err) }

	year := admin.query['y']
	month := admin.query['m']

	if year == '' {
		return admin.redirect('/admin/reservations-$src')
	} else {
		return admin.redirect('/admin/reservations-calendar?y=$year&m=$month')
	}
}

// post_admin_calender handles post of reservation calendar
['/reservations-calendar'; post]
pub fn (mut admin Admin) post_admin_calender(m int, y int) vweb.Result {
	rooms := admin.service_get_all_rooms() or { panic(err) }

	mut now := time.now()

	if m != 0 && y != 0 {
		now = time.Time{
			year: y
			month: m
			day: 01
		}
	}

	g := unsafe { admin.form }

	for _, x in rooms {
		cur_map := admin.service_get_blocked_rooms(now) or { panic(err) }
		for name, value in cur_map[x.id - 1] {
			if value > 0 && cur_map[x.id - 1][name] != g['remove_block_' + x.id.str() + '_' + name[0..10]].int() {
				admin.service_delete_block_by_id(value) or { panic(err) }
			}
		}
	}

	for name, _ in g {
		gh := name.len
		if gh > 9 && name[0..9] == 'add_block' {
			room_id := name[10..11].int()
			date := time.parse(name[12..gh] + ' 00:00:00') or { panic(err) }
			admin.service_add_block(room_id, date) or { panic(err) }
		}
	}

	return admin.redirect('/admin/reservations-calendar?y=$y&m=$m')
}
