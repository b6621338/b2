module main

import vweb
import databases

['/controller/reservation'; post]
pub fn (mut app App) controller_create_reservation(first_name string, last_name string, email string, phone string, start_date string, end_date string, room_id string) vweb.Result {
	if first_name == '' {
		app.set_status(400, '')
		return app.text('First Name cannot be empty')
	}
	if last_name == '' {
		app.set_status(400, '')
		return app.text('Last Name cannot be empty')
	}
	if email == '' {
		app.set_status(400, '')
		return app.text('email cannot be empty')
	}
	if phone == '' {
		app.set_status(400, '')
		return app.text('Phone Number cannot be empty')
	}
	app.service_add_reservation(first_name, last_name, email, phone, start_date, end_date, room_id) or {
		app.set_status(400, '')
		return app.text('error: ${err}')
	}

	// Notify the user of the reservation
	app.my_smtp(first_name, email, start_date, end_date)

	// Notify the hotel of the reservation
	app.hotel_smtp(first_name, last_name, start_date, end_date)

	mut red := databases.setup()

	red.set('first_name', first_name)
	red.set('last_name', last_name)
	red.set('email', email)
	red.set('phone', phone)
	red.set('start_date', start_date)
	red.set('end_date', end_date)

	red.del('check_in') or { panic(err) }
	red.del('check_out') or { panic(err) }

	app.set_status(201, '')
	return app.text('Reservation created successfully')
}
