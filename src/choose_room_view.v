module main

import vweb
import databases

['/rooms'; get]
pub fn (mut app App) rooms() vweb.Result {
	mut redis := databases.setup()

	token := app.get_cookie('token') or { '0' }.int()

	redis.del('error') or { panic(err) }
	check_in := redis.get('check_in') or { panic(err) }
	check_out := redis.get('check_out') or { panic(err) }

	rooms := app.services_get_all_available_rooms_by_dates(check_in, check_out) or {
		eprintln(err)
		panic(err)
	}

    return $vweb.html()
}

['/rooms/:id'; get]
pub fn (mut app App) room (id string) vweb.Result {
	mut red := databases.setup()

	red.set('room_id', id)

	return app.redirect('/reservations')
}
