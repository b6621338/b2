module main

import time

// add_date adds r years, months, and days to t and returns the resulting time.
fn add_date(t time.Time, r int) time.Time {
	m := t.month + r
	m1 := m%12
	if m > 12 {
		y1 := m/12
		y := t.year + y1

		d := time.Time{
			year: y
			month: m1
			day: t.day
		}

		return d

	} else if m <= 0 {
		y1 := m/12
		y := t.year + y1 - 1

		d := time.Time{
			year: y
			month: 12 + m1
			day: t.day
		}

		return d

	} else if m == 12 {
		d := time.Time{
			year: t.year
			month: m
			day: t.day
		}
		return d
	}

	d := time.Time{
		year: t.year
		month: m1
		day: t.day
	}

	return d
}
