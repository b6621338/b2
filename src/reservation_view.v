module main

import vweb
import databases

['/reservations'; get]
pub fn (mut app App) reservations() vweb.Result {
	mut red := databases.setup()
	defer {
		databases.cleanup(mut red)
	}

	token := app.get_cookie('token') or { '0' }.int()

	room_id := red.get('room_id') or { panic(err) }
	check_in := red.get('check_in') or { panic(err) }
	check_out := red.get('check_out') or { panic(err) }
	room_name := app.services_get_room_name_by_id(room_id) or { panic(err) }

	return $vweb.html()
}
