module main

import databases
import db.sqlite
import time

// services_get_all_available_rooms_by_dates returns all rooms that are not booked within the given date range
fn (mut app App) services_get_all_available_rooms_by_dates (start_date string, end_date string) ![]sqlite.Row {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	sd := app.services_format_time(start_date) or { panic(err) }
	ed := app.services_format_time(end_date) or { panic(err) }

	mut params := [sd, ed]

	rooms := db.exec_param_many('SELECT r.id, r.room_name FROM rooms r WHERE r.id NOT IN (SELECT rr.room_id FROM roomrestrictions rr WHERE ? < rr.end_date AND ? > rr.start_date);', params) or {
		panic(err)
	}
	return rooms
}

// services_get_room_name_by_id returns the room name for the given room id
fn (mut app App) services_get_room_name_by_id (room_id string) !string {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	results := sql db {
		select from Room where id == room_id.int()
	}!
	room := results[0].room_name
	return room
}

// services_format_time services_format_time formats the time string to be used by time.parse_format()
fn (mut app App) services_format_time (t string) !string {
	tlen := t.len
	t1 := t[4..tlen]
	t2 := time.parse_format(t1, 'MMM DD YYYY') or { panic(err) }
	t3 := t2.str()
	return t3
}

// service_add_rooms adds a list of restrictions when required
fn (mut app App) service_add_rooms() ! {
	mut db := databases.create_db_connection()!
	defer {
		db.close() or { panic(err) }
	}

	rooms := ['European Room', 'Asiatic Room', 'African Room', 'South American Room']

	for room in rooms {

		room_model := Room{
			room_name: room
		}

		mut insert_error := ''
		sql db {
			insert room_model into Room
		} or { insert_error = err.msg() }
		if insert_error != '' {
			return error(insert_error)
		}
	}
}

fn (mut admin Admin) service_get_all_rooms() ![]Room {
	mut db := databases.create_db_connection()!
	defer {
		db.close() or { panic(err) }
	}

	results := sql db {
		select from Room
	}!
	return results
}

fn (mut admin Admin) service_get_blocked_rooms(now time.Time) ![]map[string]int {
	first_of_month := time.Time{
		year: now.year
		month: now.month
		day: 01
	}
	last_of_month := time.Time{
		year: now.year
		month: now.month
		day: time.days_in_month(now.month, now.year) or { panic(err) }
	}

	rooms := admin.service_get_all_rooms() or { panic(err) }

	mut res_map := []map[string]int{}
	mut blk_map := []map[string]int{}

	for _, x in rooms {
		mut reservation_map := map[string]int{}
		mut block_map := map[string]int{}

		for d in first_of_month.day..last_of_month.day {
			f := time.Time{
				year: now.year
				month: now.month
				day: d
			}
			reservation_map[f.format()] = 0
			block_map[f.format()] = 0
		}

		restrictions := admin.service_get_restriction_for_room_by_id(x.id, first_of_month, last_of_month) or { panic(err) }

		for _, y in restrictions {
			if y.reservation_id > 0 {
				// println(y.start_date[8..10])
				for d in y.start_date[8..10].int()..y.end_date[8..10].int() {
					f := time.Time{
						year: now.year
						month: now.month
						day: d
					}
					reservation_map[f.format()] = y.reservation_id
				}
			} else {
				sd := y.start_date[0..10]
				block_map['$sd 00:00'] = y.id
			}
		}
		res_map << reservation_map
		blk_map << block_map

	}

	return blk_map
}

// service_add_block inserts a whole room restriction
fn (mut admin Admin) service_add_block (id int, start_date time.Time) ! {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	room_res_model := RoomRestriction{
		start_date: start_date.str()
		end_date: start_date.add_days(1).str()
		room_id: id
		reservation_id: 0
		restriction_id: 2
	}

	sql db {
		insert room_res_model into RoomRestriction
	} or { panic(err) }
}

// service_delete_block_by_id inserts a whole room restriction
fn (mut admin Admin) service_delete_block_by_id (id int) ! {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	sql db {
		delete from RoomRestriction where id == id
	} or { panic(err) }
}
