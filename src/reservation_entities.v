module main

[table: 'reservations']
pub struct Reservation {
mut:
	id         		 int       			[primary; sql: serial]
	firstname  		 string    			[sql_type: 'TEXT']
	lastname   		 string    			[sql_type: 'TEXT']
	email      		 string    			[sql_type: 'TEXT']
	phone      		 string    			[sql_type: 'TEXT']
	start_date 		 string    			[sql_type: 'TEXT']
	end_date   		 string    			[sql_type: 'TEXT']
	room_id    		 int       			[sql_type: 'INTEGER']
	created_at 		 string    		   	[default: 'CURRENT_TIMESTAMP']
	updated_at 		 string    		   	[default: 'CURRENT_TIMESTAMP']
	processed		 int				[default: 0]
	room_restriction []RoomRestriction 	[fkey: 'reservation_id']
}
