module main

import databases
import time

// service_add_room_restriction adds a room restriction to the database
fn (mut app App) service_add_room_restriction (sd string, ed string, room_id int, reservation_id int) ! {
	mut db := databases.create_db_connection()!
	defer {
		db.close() or { panic(err) }
	}

	now := time.now()
	room_restriction_model := RoomRestriction {
		start_date: sd
		end_date: ed
		room_id: room_id
		reservation_id: reservation_id
		created_at: now.str()
		updated_at: now.str()
		restriction_id: 1
	}

	mut insert_error := ''
	sql db {
		insert room_restriction_model into RoomRestriction
	} or { insert_error = err.msg() }
	if insert_error != '' {
		return error(insert_error)
	}
}

// service_add_restrictions adds a list of restrictions when required
fn (mut app App) service_add_restrictions() ! {
	mut db := databases.create_db_connection()!
	defer {
		db.close() or { panic(err) }
	}

	restriction := ['Restriction', 'Owner Block']

	for res in restriction {

		restriction_model := Restriction{
			restriction_name: res
		}

		mut insert_error := ''
		sql db {
			insert restriction_model into Restriction
		} or { insert_error = err.msg() }
		if insert_error != '' {
			return error(insert_error)
		}
	}
}

// service_get_restriction_for_room_by_id gets all restrictions for a room by id
fn (mut admin Admin) service_get_restriction_for_room_by_id (room_id int, start time.Time, end time.Time) ![]RoomRestriction {
	mut db := databases.create_db_connection()!
	defer {
		db.close() or { panic(err) }
	}

	result := sql db {
		select from RoomRestriction
		where end_date > start.format() && start_date < end.format() && room_id == room_id
	} or { panic(err) }

	return result
}
