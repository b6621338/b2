module main

[table: 'users']
pub struct User {
mut:
	id       	 int       [primary; sql: serial]
	firstname 	 string    [sql_type: 'TEXT']
	lastname 	 string    [sql_type: 'TEXT']
	email 		 string    [sql_type: 'TEXT']
	password     string    [sql_type: 'TEXT']
	created_at   string    [default: 'CURRENT_TIMESTAMP']
	updated_at   string    [default: 'CURRENT_TIMESTAMP']
	access_level int   	   [sql_type: 'INT']
}
