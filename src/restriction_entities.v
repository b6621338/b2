module main

[table: 'restrictions']
pub struct Restriction {
mut:
	id       		 int       		   [primary; sql: serial]
	restriction_name string    		   [sql_type: 'TEXT']
	created_at 		 string    		   [default: 'CURRENT_TIMESTAMP']
	updated_at 		 string    		   [default: 'CURRENT_TIMESTAMP']
	room_restriction []RoomRestriction [fkey: 'restriction_id']
}
