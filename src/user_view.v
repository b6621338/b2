module main

import vweb

['/login'; get]
pub fn (mut app App) login() vweb.Result {
	token := app.get_cookie('token') or { '0' }.int()

	return $vweb.html()
}

['/logout'; get]
pub fn (mut app App) logout() vweb.Result {
	app.set_cookie(name: 'token', value: '0')

	return app.redirect('/login')
}
