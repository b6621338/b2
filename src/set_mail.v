module main

import net.smtp
import strconv
import os

// my_smtp is a function that sends an email to the user
fn (mut app App) my_smtp(first_name string, email string, start_date string, end_date string) {
	html_message := unsafe{strconv.v_sprintf('
		<string>Reservation Conformation</strong><br>
		Dear %s: <br>
		This is to confirm your reservation from %s to %s.
		', first_name, start_date, end_date)}

	hm := os.read_file(os.resource_abs_path('.') + '/email-templates/basic.html') or { panic(err) }
	hm1 := hm.replace('{%body%}', html_message)

	send_cfg := smtp.Mail{
		to: email
		subject: 'Reservation Successful'
		body_type: smtp.BodyType.html
		body: hm1
	}

	app.client.send(send_cfg) or { panic(err) }
}

// hotel_smtp is a function that sends an email to the hotel
fn (mut app App) hotel_smtp(first_name string, last_name string, start_date string, end_date string) {
	html_message := unsafe{strconv.v_sprintf('
		<string>Reservation Notification</strong><br>
		New reservation from %s %s from %s to %s.
		', first_name, last_name, start_date, end_date)}

	send_cfg := smtp.Mail{
		to: "13thab@skiff.com"
		subject: 'Reservation Notification'
		body_type: smtp.BodyType.html
		body: html_message
	}

	app.client.send(send_cfg) or { panic(err) }
}
