module main

[table: 'roomrestrictions']
pub struct RoomRestriction {
mut:
	id       	   int       [primary; sql: serial]
	start_date     string    [sql_type: 'TEXT']
	end_date 	   string    [sql_type: 'TEXT']
	room_id 	   int
	reservation_id int
	created_at 	   string    [default: 'CURRENT_TIMESTAMP']
	updated_at 	   string    [default: 'CURRENT_TIMESTAMP']
	restriction_id int
}
