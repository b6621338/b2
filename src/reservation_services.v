module main

import databases
import db.sqlite
import time

// service_add_reservation adds a reservation to the database
fn (mut app App) service_add_reservation (first_name string, last_name string, email string, phone string, start_date string, end_date string, room_id string) ! {
	mut db := databases.create_db_connection()!
	defer {
		db.close() or { panic(err) }
	}

	sd := app.services_format_time(start_date) or { panic(err) }
	ed := app.services_format_time(end_date) or { panic(err) }
	rid := room_id.int()
	reservation_model := Reservation {
		firstname: first_name
		lastname: last_name
		email: email
		phone: phone
		start_date: sd
		end_date: ed
		room_id: rid
	}

	mut insert_error := ''
	sql db {
		insert reservation_model into Reservation
	} or { insert_error = err.msg() }

	res_id := db.last_insert_rowid()
	res_id1 := res_id.str()
	res_id2 := res_id1.int()
	app.service_add_room_restriction(sd, ed, rid, res_id2) or { panic(err) }

	if insert_error != '' {
		return error(insert_error)
	}
}

// service_all_reservations shows all reservations
fn (mut app Admin) service_all_reservations() ![]sqlite.Row {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	query := '
		select r.id, r.firstname, r.lastname, r.email, r.phone, r.start_date,
		r.end_date, r.room_id, r.created_at, r.updated_at, r.processed,
		rm.id, rm.room_name
		from reservations r
		left join rooms rm on (r.room_id = rm.id)
		order by r.start_date asc
'

	reservations := db.exec(query) or { panic(err) }
	return reservations
}

// service_all_new_reservations shows all new reservations
fn (mut app Admin) service_all_new_reservations() ![]sqlite.Row {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	query := '
		select r.id, r.firstname, r.lastname, r.email, r.phone, r.start_date,
		r.end_date, r.room_id, r.created_at, r.updated_at, r.processed,
		rm.id, rm.room_name
		from reservations r
		left join rooms rm on (r.room_id = rm.id)
		where processed = 0
		order by r.start_date asc
'

	reservations := db.exec(query) or { panic(err) }
	return reservations
}

// service_get_reservation_by_id gets a reservation by id
fn (mut app Admin) service_get_reservation_by_id(id int) !sqlite.Row {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	query := '
		select r.id, r.firstname, r.lastname, r.email, r.phone, r.start_date,
		r.end_date, r.room_id, r.created_at, r.updated_at, r.processed,
		rm.id, rm.room_name
		from reservations r
		left join rooms rm on (r.room_id = rm.id)
		where r.id = $id
'

	row := db.exec_one(query) or { panic(err) }
	return row
}

// service_update_reservation updates a reservation
fn (mut admin Admin) service_update_reservation(reservation Reservation) ! {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	sql db {
		update Reservation set firstname = reservation.firstname,
		lastname = reservation.lastname,
		email = reservation.email,
		phone = reservation.phone,
		updated_at = time.now().str()
		where id == reservation.id
	} or { panic(err) }
}

// service_delete_reservation deletes a reservation by id
fn (mut admin Admin) service_delete_reservation(id int) ! {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	sql db {
		delete from Reservation where id == id
	} or { panic(err) }

	sql db {
		delete from RoomRestriction where reservation_id == id
	} or { panic(err) }
}

// service_update_reservation_processed updates a reservation to processed
fn (mut admin Admin) service_update_reservation_processed(id int) ! {
	mut db := databases.create_db_connection() or { panic(err) }
	defer {
		db.close() or { panic(err) }
	}

	sql db {
		update Reservation set processed = 1 where id == id
	} or { panic(err) }
}
