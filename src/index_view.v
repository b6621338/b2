module main

import vweb
import databases

['/'; get]
pub fn (mut app App) index() vweb.Result {
	mut red := databases.setup()
	defer {
		databases.cleanup(mut red)
	}

	token := app.get_cookie('token') or { '0' }.int()

	err := red.get('error') or { '' }
	start_date := red.get('check_in') or { '' }
	end_date := red.get('check_out') or { '' }

    return $vweb.html()
}

['/'; post]
pub fn (mut app App) post_index(check_in string, check_out string) vweb.Result {
	mut red := databases.setup()

	red.set('check_in', check_in)
	red.set('check_out', check_out)

	rooms := app.services_get_all_available_rooms_by_dates(check_in, check_out) or {
		eprintln(err)
		panic(err)
	}

	if rooms.len == 0 {
		err := 'No rooms available for the selected dates'
		red.set('error', err)
		return app.redirect('/')
	}

	return app.redirect('/rooms')
}
