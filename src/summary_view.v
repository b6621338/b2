module main

import vweb
import databases

['/reservation-summary'; get]
pub fn (mut app App) summary () vweb.Result {
	mut red := databases.setup()
	defer {
		databases.cleanup(mut red)
	}

	token := app.get_cookie('token') or { '0' }.int()

	first_name := red.get('first_name') or { panic(err) }
	last_name := red.get('last_name') or { panic(err) }
	email := red.get('email') or { panic(err) }
	phone := red.get('phone') or { panic(err) }
	room_id := red.get('room_id') or { panic(err) }
	check_in := red.get('start_date') or { panic(err) }
	check_out := red.get('end_date') or { panic(err) }
	room_name := app.services_get_room_name_by_id(room_id) or { panic(err) }

	return $vweb.html()
}
