module main

[table: 'rooms']
pub struct Room {
mut:
	id         		 int   			   [primary; sql: serial]
	room_name  		 string    		   [sql_type: 'TEXT']
	created_at 		 string    		   [default: 'CURRENT_TIMESTAMP']
	updated_at 		 string    		   [default: 'CURRENT_TIMESTAMP']
	room_restriction []RoomRestriction [fkey: 'room_id']
}
