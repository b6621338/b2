# Bed and Breakfast

This is the repository for my bookings and reservations project.

## Getting Started

Download links:

SSH clone URL: ssh://git@gitlab.com:b6621338/b2.git

HTTPS clone URL: https://gitlab.com/b6621338/b2.git



These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites

Must have [V](https://vlang.io) version 0.4.3 or higher installed.

## Dependencies

- [vweb](https://modules.vlang.io/vweb.html)
- [db.sqlite](https://modules.vlang.io/db.sqlite.html)
- [time](https://modules.vlang.io/time.html)
- [patrickpissurno.vredis](https://github.com/patrickpissurno/vredis)
- [crypto.bcrypt](https://modules.vlang.io/crypto.bcrypt.html)
- [net.smtp](https://modules.vlang.io/net.smtp.html)
