module mails

import net.smtp

// setup a client to send emails
pub fn setup() &smtp.Client {
	client_cfg := smtp.Client{
		server: 'localhost'
		port: 1025
		from: 'me@me.me'
	}

	mut client := smtp.new_client(client_cfg) or { panic(err) }

	return client
}

pub fn send_mail(mut client &smtp.Client) ! {
	html_message := '
		<string>Reservation Conformation</strong><br>
		Dear first_name:, <br>
		This is to confirm your reservation from start_date to end_date.'

	send_cfg := smtp.Mail{
		to: 'email@email.mail'
		subject: 'Reservation Successful'
		body_type: smtp.BodyType.html
		body: html_message
	}

	client.send(send_cfg) or { panic(err) }
}
