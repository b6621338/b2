module databases

pub fn setup() Redis {
	mut r := connect(ConnOpts{}) or { panic(err) }
	return r
}

pub fn cleanup(mut r Redis) {
	r.flushall()
	r.disconnect()
}
